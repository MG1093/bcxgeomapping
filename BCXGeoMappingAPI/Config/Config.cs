﻿using NLog;
using System.Collections.Generic;
using System.Net;

namespace BCXGeoMappingAPI.Config
{
    public class APIHelperClass
    {
        // this must appear before other static instance types.
        public static List<APIHelperClass> APIsAll { get; } = new List<APIHelperClass>();
        public string Name { get; set; }
        public string Value { get; set; }
        public static APIHelperClass GoogleMapsLocationURI { get; } = new APIHelperClass(nameof(GoogleMapsLocationURI), "/maps/api/geocode/json?");


        public static APIHelperClass Custom
        {
            get => new APIHelperClass(nameof(Custom), "/custom");
            set => new APIHelperClass(Custom.Name, Custom.Value);
        }
        private APIHelperClass(string name, string value)
        {
            Name = name;
            Value = value;
            APIsAll.Add(this);
        }
    }
    public class ConnectionTypeHelperClass
    {
        // this must appear before other static instance types.
        public static List<ConnectionTypeHelperClass> ConnectionTypesAll { get; } = new List<ConnectionTypeHelperClass>();
        public string Name { get; set; }
        public string Value { get; set; }

        public static ConnectionTypeHelperClass HTTPS { get; } = new ConnectionTypeHelperClass(nameof(HTTPS), "https://");
        public static ConnectionTypeHelperClass HTTP { get; } = new ConnectionTypeHelperClass(nameof(HTTP), "http://");
        public static ConnectionTypeHelperClass Custom
        {
            get => new ConnectionTypeHelperClass(nameof(Custom), "");
            set => new ConnectionTypeHelperClass(Custom.Name, Custom.Value);
        }
        private ConnectionTypeHelperClass(string name, string value)
        {
            Name = name;
            Value = value;
            ConnectionTypesAll.Add(this);
        }
    }
    public class DomainNameHelperClass
    {
        // this must appear before other static instance types.
        public static List<DomainNameHelperClass> DomainNamesAll { get; } = new List<DomainNameHelperClass>();
        public string Name { get; set; }
        public string Value { get; set; }

        public static DomainNameHelperClass GoogleMaps { get; } = new DomainNameHelperClass(nameof(GoogleMaps), "maps.googleapis.com");
        public static DomainNameHelperClass Custom
        {
            get => new DomainNameHelperClass(nameof(Custom), "");
            set => new DomainNameHelperClass(Custom.Name, Custom.Value);
        }
        private DomainNameHelperClass(string name, string value)
        {
            Name = name;
            Value = value;
            DomainNamesAll.Add(this);
        }
    }
    public class LoggerHelperClass
    {
        // this must appear before other static instance types.
        public string Name { get; set; }
        public ILogger Value { get; set; }

        public static LoggerHelperClass DefaultLogger { get; } = new LoggerHelperClass(nameof(DefaultLogger), LogManager.GetCurrentClassLogger());
        public static LoggerHelperClass CustomLogger
        {
            get => new LoggerHelperClass(nameof(CustomLogger), LogManager.GetLogger(""));
            set => new LoggerHelperClass(CustomLogger.Name, CustomLogger.Value);
        }

        private LoggerHelperClass(string name, ILogger value)
        {
            Name = name;
            Value = value;
        }
    }
    public class ParameterHelperClass
    {
        // this must appear before other static instance types.
        public static List<ParameterHelperClass> ParametersAll { get; } = new List<ParameterHelperClass>();
        public string Name { get; set; }
        public string Value { get; set; }

        public static ParameterHelperClass AutenticateEmail { get; set; } = new ParameterHelperClass("emailAddress", "ucs@bcxlabs1.onmicrosoft.com");
        public static ParameterHelperClass AutenticatePassword { get; set; } = new ParameterHelperClass("password", "ZAQ!@wsx3456");

        public static Dictionary<string, string> AgentLoginBodyDict = new Dictionary<string, string>()
        {
            { AutenticateEmail.Name, AutenticateEmail.Value},
            { AutenticatePassword.Name, AutenticatePassword.Value},
        
        };

        public static ParameterHelperClass Custom
        {
            get => new ParameterHelperClass(nameof(Custom), "");
            set => new ParameterHelperClass(Custom.Name, Custom.Value);
        }
        private ParameterHelperClass(string name, string value)
        {
            Name = name;
            Value = value;
            ParametersAll.Add(this);
        }
    }
    public class URIHelperClass
    {
        // this must appear before other static instance types.
        public static List<URIHelperClass> URIsAll { get; } = new List<URIHelperClass>();
        public string Name { get; set; }
        public string Value { get; set; }

        public static URIHelperClass GoogleKey { get; set; } = new URIHelperClass(nameof(GoogleKey), "&key=AIzaSyAjhVvmwW3Tw0oMQTYS4OMOOqz1Bb5gOYw");

        public static URIHelperClass Custom
        {
            get => new URIHelperClass(nameof(Custom), "/custom");
            set => new URIHelperClass(Custom.Name, Custom.Value);
        }
        private URIHelperClass(string name, string value)
        {
            Name = name;
            Value = value;
            URIsAll.Add(this);
        }
    }
}
