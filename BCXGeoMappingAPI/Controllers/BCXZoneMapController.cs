﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BCXGeoMappingAPI.Config;
using BCXGeoMappingAPI.DBConext;
using BCXGeoMappingAPI.Helpers;
using BCXGeoMappingAPI.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;


namespace BCXGeoMappingAPI.Controllers
{
    [Route("api/v1/[Controller]")]
    [ApiController]
    public class BCXZoneMapController : ControllerBase
    {
        //Post: Create Customer with an Address and Contact associated to it.
        //api/v1/BCXZoneMap/BatchGeoCode
        /* Body Data Format Expected
        {
        "addresses": [
            "7221 Blue Blossom St, Centurion, South Africa",
            "7222 Red Blossom St, Centurion, South Africa",
            "7223 Green Blossom St, Centurion, South Africa",
            "7224 Black Blossom St, Centurion, South Africa",
            "7225 Orange Blossom St, Centurion, South Africa"
        ]
        }
        */
        [HttpPost]
        [Route("BatchGeoCode")]
        public async Task<IActionResult> BatchGeoCode([FromBody]JObject addresses)
        {
            CustomHelpers helpers = new CustomHelpers();
            var addressesDeserialized = JsonConvert.DeserializeObject<Addresses>(addresses.ToString());

            List<GoogleMapResult> googleMapResult = new List<GoogleMapResult>();
            try
            {
                foreach (var address in addressesDeserialized.addresses)
                {
                    var client = new RestClient("https://maps.googleapis.com/maps/api/geocode/json?" + "address=" + address + "&key=AIzaSyAjhVvmwW3Tw0oMQTYS4OMOOqz1Bb5gOYw");
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Content-Type", "application/json");
                    IRestResponse response = await client.ExecuteTaskAsync(request);                
                    googleMapResult.Add(JsonConvert.DeserializeObject<GoogleMapResult>(response.Content));                   
                }
                return Ok(googleMapResult);
            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }
    }
}


