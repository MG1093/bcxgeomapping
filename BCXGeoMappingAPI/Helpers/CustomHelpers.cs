﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using BCXGeoMappingAPI.Config;
using System.IO;
using RestSharp;
using System.Diagnostics;
using NLog;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Net.NetworkInformation;
using Microsoft.Extensions.Configuration;

namespace BCXGeoMappingAPI.Helpers
{
    public class CustomHelpers
    {
        public ILogger Logger { get; set; } = LoggerHelperClass.DefaultLogger.Value;


        /////////////////////////////////
        // Creating Directories     ////
        ///////////////////////////////   
        public void CreateDirectoriesWindows()
        {
            try
            {
                var currentDirectory = Directory.GetCurrentDirectory();
                var createConfigDirectory = Directory.CreateDirectory(currentDirectory + "\\Config");
                var createLogDirectory = Directory.CreateDirectory(currentDirectory + "\\Logs");
            }
            catch (Exception error)
            {
                Logger.Error(error);
                throw error;
            }
        }

        /////////////////////////////////
        // Encryption and Hashing    ///
        ///////////////////////////////   

        public string EncryptString(string plainText)
        {
            try
            {
                if (string.IsNullOrEmpty(plainText))
                {
                    plainText = "No Data Received";
                }
                byte[] IV, Key;

                using (Aes myAes = Aes.Create())
                {
                    Key = myAes.Key;
                    IV = myAes.IV;
                }

                // Check arguments.
                if (plainText == null || plainText.Length <= 0)
                    throw new ArgumentNullException("plainText");
                if (Key == null || Key.Length <= 0)
                    throw new ArgumentNullException("Key");
                if (IV == null || IV.Length <= 0)
                    throw new ArgumentNullException("IV");
                byte[] encrypted;
                // Create an Aes object
                // with the specified key and IV.
                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = Key;
                    aesAlg.IV = IV;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                    // Create the streams used for encryption.
                    using (MemoryStream msEncrypt = new MemoryStream())
                    {
                        using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                            {

                                //Write all data to the stream.
                                swEncrypt.Write(plainText);
                            }
                            encrypted = msEncrypt.ToArray();
                        }
                    }
                    string outEncrypted = System.Convert.ToBase64String(encrypted);
                    string outIV = System.Convert.ToBase64String(IV);
                    string outKey = System.Convert.ToBase64String(Key);

                    List<string> myArr = new List<string>();
                    myArr.Add(outKey);
                    myArr.Add(outIV);
                    myArr.Add(outEncrypted);

                    string returns = string.Join("", myArr);



                    return returns;
                }
            }
            catch (Exception error)
            {
                Logger.Error(error);
                throw error;
            }
        }
        public string DecryptString(string data)
        {
            try
            {
                string k = data.Substring(0, 44);
                string i = data.Substring(44, 24);
                string d = data.Substring(68, data.Length - 68);

                byte[] Key = System.Convert.FromBase64String(k);
                byte[] IV = System.Convert.FromBase64String(i);
                byte[] cipherText = System.Convert.FromBase64String(d);

                // Check arguments.
                if (cipherText == null || cipherText.Length <= 0)
                    throw new ArgumentNullException("cipherText");
                if (Key == null || Key.Length <= 0)
                    throw new ArgumentNullException("Key");
                if (IV == null || IV.Length <= 0)
                    throw new ArgumentNullException("IV");

                // Declare the string used to hold
                // the decrypted text.
                string plaintext = null;

                // Create an Aes object
                // with the specified key and IV.
                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = Key;
                    aesAlg.IV = IV;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                    // Create the streams used for decryption.
                    using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            {

                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                return plaintext;
            }
            catch (Exception error)
            {
                Logger.Error(error);
                throw error;
            }
        }
        public string CalculateHash(string data)
        {
            using (var md5 = MD5.Create())
            {
                var dataInBytes = Encoding.UTF8.GetBytes(data);
                var hash = md5.ComputeHash(dataInBytes);
                return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
            }
        }

        ////////////////////////
        ///  API Calls    /////
        ///////////////////////  

        public IRestClient RestClientRequestBuilder(ConnectionTypeHelperClass connectionType, DomainNameHelperClass domainName,
                                                    APIHelperClass api, bool addAutoCookieHandling = true, int timeOutinSec = 1000)
        {
            RestClient URL = null;
            CookieContainer cookieContainer = null;

            try
            {
                //Build URL
                URL = new RestClient
                (
                    connectionType.Value +
                    domainName.Value +
                    api.Value
                )
                {
                    //Overide default timeout value of URl when RestClient is called.
                    Timeout = timeOutinSec * 100
                };

                //Add Proxy if in appsetting
                var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
                if (!(String.IsNullOrWhiteSpace(configuration["Proxy"])))
                {
                    URL.Proxy = new WebProxy(configuration["Proxy"]);
                }

                //Add auto cookie handling.
                if (addAutoCookieHandling)
                    cookieContainer = URL.CookieContainer = new CookieContainer();

                return URL;
            }
            catch (Exception error)
            {
                Logger.Error(
                            "{0}: {1}: Error Occured: {2} ",
                            DateTime.Now,
                            nameof(RestClientRequestBuilder),
                            error);

                Console.WriteLine("Something went wrong AgentGetConfig");
                return URL;
            }
        }

        public async Task<IRestResponse> RestApiCallerAsync(IRestClient restClient, Method requestType,
                                           URIHelperClass uri, bool bypassCert = true,
                                           bool addTokenToHeader = false, Dictionary<string, string> tokens = null,
                                           bool addBody = false, string jsonBody = null, bool addQueryString = false,
                                           Dictionary<string, string> queryString = null, bool addUrlSegment = false,
                                           string urlSegment = null, string uriAddOn = "")
        {
            IRestResponse response = null;
            RestRequest requestContainer = null;
            try
            {
                int counter = 0;
                tryagain:
                //Bypass SSL cert if you choose
                if (bypassCert)
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

         
        

                //Create a request container that will include, uri, requestType like Get or Post.
                requestContainer = new RestRequest(uri.Value + uriAddOn, requestType);

                //Add token to header to request container
                if (addTokenToHeader)
                    foreach (var item in tokens) { requestContainer.AddParameter(item.Key, item.Value, ParameterType.HttpHeader); }
                //Add json body to request container
                if (addBody)
                { requestContainer.AddParameter("application/json", jsonBody, ParameterType.RequestBody); }
                //Add querystring to request container
                if (addQueryString)
                    foreach (var item in queryString) { requestContainer.AddParameter(item.Key, item.Value, ParameterType.QueryString); }
                //Add URL Segment to request container
                if (addUrlSegment)
                    requestContainer.AddParameter(urlSegment, ParameterType.UrlSegment);

                //Do API call 
                response = await restClient.ExecuteTaskAsync(requestContainer);

                // response = restClient.Execute(requestContainer);
                if (response.StatusCode == 0)
                {
                    if (counter < 2)
                    {
                        counter++;
                        goto tryagain;
                    }
                }
                //Console.WriteLine("API Call Response Status Code {0} and Request {1}",
                //    response.StatusCode,
                //    requestContainer.Resource);

                return response;
            }
            catch (Exception error)
            {
                Logger.Error(
                            "{0}: {1}: Error Occured: {2} ",
                            DateTime.Now,
                            nameof(RestApiCaller),
                            error);

                Console.WriteLine("API Call Response Status Code {0} and URI {1}",
                                    response.StatusCode,
                                    requestContainer.Resource);
                return response;
            }
        }

        public IRestResponse RestApiCaller(IRestClient restClient, Method requestType,
                                   URIHelperClass uri, bool bypassCert = true,
                                   bool addTokenToHeader = false, Dictionary<string, string> tokens = null,
                                   bool addBody = false, string jsonBody = null, bool addQueryString = false,
                                   Dictionary<string, string> queryString = null, bool addUrlSegment = false,
                                   string urlSegment = null, string uriAddOn = "")
        {
            IRestResponse response = null;
            RestRequest requestContainer = null;
            try
            {
                int counter = 0;
                tryagain:
                //Bypass SSL cert if you choose
                if (bypassCert)
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

                //Create a request container that will include, uri, requestType like Get or Post.
                requestContainer = new RestRequest(uri.Value + uriAddOn, requestType);

                //Add token to header to request container
                if (addTokenToHeader)
                    foreach (var item in tokens) { requestContainer.AddParameter(item.Key, item.Value, ParameterType.HttpHeader); }
                //Add json body to request container
                if (addBody)
                { requestContainer.AddParameter("application/json", jsonBody, ParameterType.RequestBody); }
                //Add querystring to request container
                if (addQueryString)
                    foreach (var item in queryString) { requestContainer.AddParameter(item.Key, item.Value, ParameterType.QueryString); }
                //Add URL Segment to request container
                if (addUrlSegment)
                    requestContainer.AddParameter(urlSegment, ParameterType.UrlSegment);

                //Do API call 
                response = restClient.Execute(requestContainer);
                if (response.StatusCode == 0)
                {
                    if (counter < 2)
                    {
                        counter++;
                        goto tryagain;
                    }
                }
                //Console.WriteLine("API Call Response Status Code {0} and Request {1}",
                //    response.StatusCode,
                //    requestContainer.Resource);

                return response;
            }
            catch (Exception error)
            {
                Logger.Error(
                            "{0}: {1}: Error Occured: {2} ",
                            DateTime.Now,
                            nameof(RestApiCaller),
                            error);

                Console.WriteLine("API Call Response Status Code {0} and URI {1}",
                                    response.StatusCode,
                                    requestContainer.Resource);
                return response;
            }
        }

        ////////////////////////////////
        ///  Working with Json    /////
        //////////////////////////////  

        public string StringToJson(Dictionary<string, string> Dict)
        {
            string jsonConvert = "";
            try
            {
                //Convert String to Json
                return jsonConvert = JsonConvert.SerializeObject(Dict);
            }
            catch (Exception error)
            {
                Logger.Error(
                              "{0}: {1}: Error Occured: {2} ",
                              DateTime.Now,
                              nameof(StringToJson),
                              error);

                return jsonConvert;
            }
        }
        public bool IsValidJson(string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return false;
            }

            var value = stringValue.Trim();

            if ((value.StartsWith("{") && value.EndsWith("}")) || //For object
                (value.StartsWith("[") && value.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(value);
                    return true;
                }
                catch (JsonReaderException)
                {
                    return false;
                }
            }
            return false;
        }

        ////////////////////////////////
        ///  Working with Lists   /////
        //////////////////////////////  

        //List Splitting helper Methods.
        public List<List<string>> SplitList(List<string> locations, int nSize = 30)
        {
            var list = new List<List<string>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }

            return list;
        }
        //List Splitting helper Methods.
        public List<List<ulong>> SplitListlong(List<ulong> locations, int nSize = 30)
        {
            var list = new List<List<ulong>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }

            return list;
        }
    }
}

